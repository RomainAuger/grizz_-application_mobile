import React from 'react';
import { Text } from 'react-native';
import { Provider } from 'mobx-react';
import { Font } from 'expo';

import Store from './src/store/store';

import Grizz from './src/grizz';

export default class App extends React.Component {
  state = {
    fontLoaded: false,
  };

  async componentDidMount() {
    await Font.loadAsync({
      'helveticaLt': require('./assets/fonts/HelveticaLt.ttf'),
    });

    this.setState({ fontLoaded: true });
  }

  render() {
    const app = this.state.fontLoaded ? <Grizz /> : <Text>Loading ...</Text>

    return (
      <Provider store={Store}>
        {app}
      </Provider>
    );
  }
}
