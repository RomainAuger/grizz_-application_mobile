# Application mobile Grizz (React Native)

Vidéo de présentation : <https://youtu.be/OMESZXvnNEo>

## Installation des prérequis

#### Installation de npm

La première étape est d'installer npm sur votre machine. Pour cela, vous devez télécharger et installer Node.js. Vous trouverez les liens de téléchargement à [cette adresse]. Pour les systèmes Debian et Ubuntu, vous pouvez également exécuter la commande suivante :

    sudo apt-get install nodejs npm

[cette adresse]: https://nodejs.org/en/download/

#### Téléchargement de l'application

Vous devez maintenant télécharger le corps de l'application. Pour cela, effectuez une copie de ce répertoire git avec la commande suivante :

    git clone https://Charles_RIO@bitbucket.org/Charles_RIO/grizz.git


#### Téléchargement des packages

Une fois npm installé et l'application téléchargée, il vous faut maintenant télécharger tous les packages npm utilisés par l'application. À savoir :

* @mapbox/polyline
* babel-plugin-transform-class-properties
* babel-plugin-transform-decorators-legacy
* expo
* mobx
* mobx-react
* react
* react-native
* react-native-display
* react-native-elements
* react-native-google-places-autocomplete

##### Téléchargement global

Cette démarche est facilitée par la présence du fichier package.json. Ce dernier contient l'ensemble des packages nécessaires et ces derniers peuvent tous être téléchargés (en une seule fois) avec la commande suivante :

    npm install

##### Téléchargement spécifique

Si vous le souhaitez, vous pouvez néanmoins télécharger les packages un par un avec la commande suivante :

    npm install nom_du_package


## Construction de l'application

La construction de l'application est très simple en React native. Nous vous renvoyons vers la documentation officielle. [Ici pour Android] et [ici pour iOS].

[Ici pour Android]: https://facebook.github.io/react-native/docs/signed-apk-android.html
[ici pour IOS]: https://facebook.github.io/react-native/docs/running-on-device.html

## Manuel d'utilisation

Nous allons, dans cette section, présenter le fonctionnement de l'application.

Au premier lancement de cette dernière, il vous sera demandé d'accepter l'autorisation de l'utilisation du GPS. Cette autorisation est, bien évidemment, fondamentale au bon fonctionnement du guidage puisque sans cette dernière, il est impossible pour l'application de connaître votre position.

Une fois cette autorisation accordée, vous accédez à l'écran d'accueil suivant, il vous suffit d'appuyer sur commencer.

![accueil](captures/accueil.jpg "accueil")

Après cela vient l'étape de la connexion aux vibreurs Bluetooth. Cette fonctionnalité n'est pas encore implémentée, l'application considérera donc que les vibreurs sont connectés et vous obtiendrez l'écran suivant :

![vibreurs](captures/connexion.jpg "vibreurs")

Appuyer ensuite sur le bouton "configuration des vibreurs". Cet écran permet à l'application de savoir quel vibreur est placé du côté droit (respectivement gauche). Comme la partie Bluetooth n'est pas encore implémentée, votre choix n'aura, pour l'instant, pas d'importance.

![config](captures/configuration.jpg "config")

Vous accédez désormais à l'écran principal de l'application. Nous allons maintenant détailler les différents éléments de l'interface :

* Tout en haut à gauche, une flèche vous permet d'effectuer un retour en arrière. Elle peut permettre, par exemple, de reconfigurer les vibreurs en cas d'erreurs.

* Vers le haut de votre écran, vous avez la possibilité de changer la position de départ de votre parcours (initialement, il s'agit de votre position actuelle), ainsi que d'entrer votre destination.

* En bas, vous pouvez choisir votre mode de déplacement parmi trois proposés (marche, vélo, course). Pour choisir un de ces modes, appuyez sur l'icône correspondante, ou, faite glisser votre doigt sur la barre.

* Une cible située en bas à droite permet de centrer la carte sur votre position actuelle (représentée par un rond bleu).

* Enfin, le bouton "Go !" vous permet de démarrer le guidage, il vous suffit alors de suivre l'itinéraire calculé.

![main](captures/main.jpg "main")

