import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, PanResponder, Animated } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { MapView, Location, Permissions } from 'expo';
import Display from 'react-native-display';
import { inject, observer } from 'mobx-react';

import NavBar from '../components/navbar';

const SCREEN_WIDTH = Dimensions.get('window').width;
const NAVBAR_HEIGHT = 55;
const NAVBAR_LENGTH = (0.85 * SCREEN_WIDTH);
const SLIDER_WIDTH = 0.25 * SCREEN_WIDTH;
const OFFSET = 2;
const DX_MAX = (0.85 * SCREEN_WIDTH)-(0.25 * SCREEN_WIDTH)-2*OFFSET;

@inject(['store'])
@observer
export default class BottomBar extends React.Component {

  render() {
    const navigation = this.props.store.navigation;

    return (
      <Display enable={!navigation} exit='fadeOut' keepAlive enter='fadeIn' style={styles.bottomBarStyle}>
        <View style={styles.backgroundStyle}>
          <View style={styles.dividerStyle} />
        </View>

        <NavBar />

        <Image
          style={styles.logoStyle}
          source={require('../../assets/img/logo.png')}
          resizeMode="contain"
        />
      </Display>
    );
  }
}

const styles = StyleSheet.create({
  bottomBarStyle: {
    width: '100%',
    height: '18%',
    alignItems: 'center',
  },
  backgroundStyle: {
    backgroundColor: '#040832',
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: NAVBAR_HEIGHT/2,
    shadowColor: 'black',
    shadowRadius: 7,
    shadowOpacity: 0.4
  },
  dividerStyle: {
    width: '100%',
    backgroundColor: '#fff',
    height: 2,
  },
  logoStyle: {
    width: '25%',
    height: '25%',
    position: 'absolute',
    bottom: '15%',
  },
});
