import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableWithoutFeedback } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { inject, observer } from 'mobx-react';

const GOOGLEMAP_API_KEY = 'AIzaSyCS7fRjjs9sjhb5EZBj_TCMZuDS-tD5-2Q';

@inject(['store'])
@observer
export default class DirectionsInput extends React.Component {
  state = {
    positionActuelle: true,
    text: '',
    nbKm: '',
  }

  getAdresses() {
    const { positionActuelle } = this.state;
    this.props.store.depart = positionActuelle ? 'Ma position' : this.refs.depart.getAddressText();
    this.props.store.destination = this.refs.destination.getAddressText();
  }

  getNbKm() {
    const { nbKm } = this.state;
    if(/^\d+$/.test(nbKm))
      return nbKm;
    return '';
  }

  render() {
    const { positionActuelle, text } = this.state;

    const departInput = (
      <GooglePlacesAutocomplete
        placeholder='Départ'
        minLength={2}
        autoFocus={true}
        returnKeyType={'default'}
        fetchDetails={true}
        styles={{
          textInputContainer: {
            backgroundColor: 'rgba(0,0,0,0)',
            borderTopWidth: 0,
            borderBottomWidth:0,
          },
          textInput: styles.textInputStyle,
          predefinedPlacesDescription: {
            color: '#1faadb'
          },
          container: {
            minHeight: 65,
            marginLeft: 25
          },
          powered: {
            height: 0
          },
          poweredContainer: {
            height: 0
          },
        }}
        currentLocation={false}
        query={{
          key: GOOGLEMAP_API_KEY,
          language: 'fr'
        }}
        ref='depart'
        textInputProps={{
          onEndEditing: () => {
            const text = this.refs.depart.getAddressText();
            if(text == ''){
              this.setState({ positionActuelle: true });
            }
          }
        }}
      />
    );

    const positionActuelleInput = (
      <TouchableWithoutFeedback onPress={() => {this.setState({positionActuelle: false});}}>
        <View style={styles.touchableViewStyle}>
          <Text style={styles.textPositionInputStyle} >Ma position</Text>
        </View>
      </TouchableWithoutFeedback>
    );

    const topInput = positionActuelle ? positionActuelleInput : departInput;

    const color = positionActuelle ? '#5AC8FA' : '#FF3B30';

    if(this.props.store.currentNavPage != 'run')
    return (
      <View style={styles.inputViewStyle}>
        <View style={[styles.inputBarViewStyle,styles.topInputStyle]}>
          <View style={[styles.circleStyle, {backgroundColor: color}]}/>
            {topInput}
        </View>

        <View style={styles.dividerInputStyle} />

        <View style={[styles.inputBarViewStyle,styles.bottomInputStyle]}>
          <View style={[styles.circleStyle, {backgroundColor: '#FF3B30'}]}/>
          <GooglePlacesAutocomplete
            placeholder='Destination'
            minLength={2}
            autoFocus={false}
            returnKeyType={'default'}
            fetchDetails={true}
            styles={{
              textInputContainer: {
                backgroundColor: 'rgba(0,0,0,0)',
                borderTopWidth: 0,
                borderBottomWidth:0,
              },
              textInput: styles.textInputStyle,
              predefinedPlacesDescription: {
                color: '#1faadb'
              },
              container: {
                minHeight: 65,
                marginLeft: 25
              },
              powered: {
                height: 0
              },
              poweredContainer: {
                height: 0
              },
            }}
            currentLocation={false}
            query={{
              key: GOOGLEMAP_API_KEY,
              language: 'fr'
            }}
            ref='destination'
          />
        </View>
      </View>
    );

    return (
      <View style={styles.inputViewStyle}>
        <View style={[{backgroundColor: 'white', paddingTop: 7},styles.bottomInputStyle,styles.topInputStyle]}>
          <View style={[styles.circleStyle, {backgroundColor: '#4CD978'}]}/>
          <TextInput
            placeholder='Nb de Km'
            style={[styles.textInputStyle]}
            value={this.state.nbKm}
            onChangeText={k => this.setState({nbKm: k})}
            keyboardType='numeric'
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  touchableViewStyle: {
    flex: 1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    marginLeft: 25
  },
  textPositionInputStyle: {
    fontFamily: 'helveticaLt',
    paddingTop: 3,
    color: '#539fc2',
    fontSize: 18,
    textAlign: 'center'
  },
  textInputStyle: {
    fontFamily: 'helveticaLt',
    color: 'black',
    fontSize: 18,
    textAlign: 'center',
    height: 55
  },
  inputViewStyle: {
    width: '90%',
    shadowColor: 'black',
    shadowRadius: 5,
    shadowOpacity: 0.07,
    borderRadius: 40,
  },
  inputBarViewStyle: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  inputStyle: {
    height: '100%',
    fontFamily: 'helveticaLt',
    color: 'black',
    fontSize: 30,
    textAlign: 'center',
    flex: 1
  },
  circleStyle: {
    position: 'absolute',
    top: 33-17/2,
    left: 15,
    width: 17,
    height: 17,
    borderRadius: 40,
  },
  topInputStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    minHeight: 65
  },
  bottomInputStyle: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  dividerInputStyle: {
    width: '100%',
    height: 1,
    backgroundColor: '#ccc'
  },
});
