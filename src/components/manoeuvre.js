import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';
import { inject, observer } from 'mobx-react';
import Display from 'react-native-display';

@inject(['store'])
@observer
export default class Manoeuvre extends React.Component {
  state = {
    enable: false,
    text: '',
  }

  componentWillMount() {
    this.props.store.manoeuvre = (txt) => this.afficheManoeuvre(txt);
  }

  afficheManoeuvre(text) {
    this.setState({text, enable: true});
    setTimeout(() => {
      this.setState({enable: false});
    },3500);
  }

  render() {
    const {enable, text} = this.state;

    return (
          <Display
            enable={enable}
            style={{
              position: 'absolute',
              backgroundColor: 'white',
              top: '10%',
              left: '10%',
              width: '80%',
              height: '10%',
              borderRadius: 10,
              shadowColor: 'black',
              shadowRadius: 5,
              shadowOpacity: 0.2,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            enter='fadeInDown'
            exit='fadeOut'
          >
            <Text style={{fontSize: 30}}>{text}</Text>
          </Display>
    );
  }
}
