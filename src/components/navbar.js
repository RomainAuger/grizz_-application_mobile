import React from 'react';
import { StyleSheet, Text, View, Dimensions, PanResponder, Animated } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { inject, observer } from 'mobx-react';

const SCREEN_WIDTH = Dimensions.get('window').width;
const NAVBAR_HEIGHT = 55;
const NAVBAR_LENGTH = (0.85 * SCREEN_WIDTH);
const SLIDER_WIDTH = 0.25 * SCREEN_WIDTH;
const OFFSET = 2;
const DX_MAX = (0.85 * SCREEN_WIDTH)-(0.25 * SCREEN_WIDTH)-2*OFFSET;

@inject(['store'])
@observer
export default class NavBar extends React.Component {
  state = {
    dx: new Animated.Value(OFFSET),
    oldDx: 0,
    walkColor: 'black',
    bicycleColor: 'black',
    runColor: 'black'
  }

  componentWillMount() {
    this.updateColor();
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
      onMoveShouldSetPanResponder: (evt, gestureState) => false,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => false,

      onPanResponderGrant: (evt, gestureState) => {
        this.setState({oldDx: this.state.dx._value - OFFSET});
        this.resetColor();
      },
      onPanResponderMove: (evt, gestureState) => {
        const { oldDx } = this.state;
        let dx = oldDx + gestureState.dx;

        if( dx < 0 )
          dx = 0;
        if( dx > DX_MAX)
          dx = DX_MAX;

        if(dx > 2*(NAVBAR_LENGTH/3) - SLIDER_WIDTH/3)
          this.props.store.setCurrentNavPage('run');
        else if(dx > (NAVBAR_LENGTH/3) - SLIDER_WIDTH/3)
          this.props.store.setCurrentNavPage('bicycle');
        else if(dx > 0)
          this.props.store.setCurrentNavPage('walk');

        this.state.dx.setValue(dx + OFFSET);
     },
     onPanResponderTerminationRequest: (evt, gestureState) => true,
     onPanResponderRelease: (evt, gestureState) => {
       // The user has released all touches while this view is the
       // responder. This typically means a gesture has succeeded
       const currentNavPage = this.props.store.currentNavPage;
       let position = OFFSET;

       if(currentNavPage == 'walk') {
         position = OFFSET;
       }
       else if(currentNavPage == 'bicycle') {
         position = NAVBAR_LENGTH/2 - SLIDER_WIDTH/2;
       }
       else if(currentNavPage == 'run') {
         position = NAVBAR_LENGTH - SLIDER_WIDTH - OFFSET;
       }

       Animated.timing(
        // Animate value over time
        this.state.dx, // The value to drive
        {
          toValue: position, // Animate to final value of 1
          duration: 300,
        }
      ).start();
      this.updateColor();
     },
     onPanResponderTerminate: (evt, gestureState) => {
       // Another component has become the responder, so this gesture
       // should be cancelled
     },
     onShouldBlockNativeResponder: (evt, gestureState) => {
       // Returns whether this component should block native components from becoming the JS
       // responder. Returns true by default. Is currently only supported on android.
       return true;
     },
   });
  }

  updateColor(p) {
    const current = this.props.store.getCurrentNavPage;
    if(current == 'walk') {
      this.setState({walkColor: 'white', bicycleColor: 'black', runColor: 'black'});
    }
    else if(current == 'bicycle') {
      this.setState({walkColor: 'black', bicycleColor: 'white', runColor: 'black'});
    }
    else if(current == 'run') {
      this.setState({walkColor: 'black', bicycleColor: 'black', runColor: 'white'});
    }
  }

  resetColor() {
    this.setState({walkColor: 'black', bicycleColor: 'black', runColor: 'black'});
  }

  moveTo(p) {
    const current = this.props.store.getCurrentNavPage;
    if(current == p) return;
    this.props.store.setCurrentNavPage(p);
    const currentNavPage = this.props.store.getCurrentNavPage;
    let position = OFFSET;

    if(currentNavPage == 'walk') {
      position = OFFSET;
    }
    else if(currentNavPage == 'bicycle') {
      position = NAVBAR_LENGTH/2 - SLIDER_WIDTH/2;
    }
    else if(currentNavPage == 'run') {
      position = NAVBAR_LENGTH - SLIDER_WIDTH - OFFSET;
    }
    Animated.timing(
     this.state.dx,
     {
       toValue: position,
       duration: 300,
     }
   ).start();
   this.updateColor();
  }

  render() {
    const currentNavPage = this.props.store.getCurrentNavPage;
    const {walkColor, bicycleColor, runColor} = this.state;

    return (
      <View style={styles.backSliderStyle} {...this._panResponder.panHandlers}>
        <Animated.View
          style={[styles.sliderStyle, {left: this.state.dx}]}
        />
        <View style={styles.iconWrapperStyle}>
          <View style={styles.iconViewStyle}>
            <Icon
              disabled={walkColor=='white' ? true : false}
              type='material-community'
              name='walk'
              size={NAVBAR_HEIGHT-20}
              color={walkColor}
              onPress={() => this.moveTo('walk')}
            />
          </View>
          <View style={styles.iconViewStyle}>
            <Icon
              disabled={bicycleColor=='white' ? true : false}
              type='font-awesome'
              name='bicycle'
              size={NAVBAR_HEIGHT-20}
              color={bicycleColor}
              onPress={() => this.moveTo('bicycle')}
            />
          </View>
          <View style={styles.iconViewStyle}>
            <Icon
              disabled={runColor=='white' ? true : false}
              type='material-community'
              name='run-fast'
              size={NAVBAR_HEIGHT-20}
              color={runColor}
              onPress={() => this.moveTo('run')}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  iconWrapperStyle: {
    marginLeft: -8,
    marginRight: -8,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconViewStyle: {
    flex: 1,
    alignItems: 'center'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backSliderStyle: {
    backgroundColor: 'white',
    height: NAVBAR_HEIGHT,
    width: 0.85 * SCREEN_WIDTH,
    borderRadius: NAVBAR_HEIGHT,
    shadowColor: 'black',
    shadowRadius: 5,
    shadowOpacity: 0.07
  },
  sliderStyle: {
    position: 'absolute',
    top: 2,
    backgroundColor: '#4CD978',
    height: NAVBAR_HEIGHT - 4,
    width: SLIDER_WIDTH,
    borderRadius: NAVBAR_HEIGHT,
  },
  textStyle: {
    fontFamily: 'helveticaLt',
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    lineHeight: 35
  },
});
