import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { inject, observer } from 'mobx-react';

@inject(['store'])
@observer
export default class RetourButton extends React.Component {
  changePage = () => {
    const current = this.props.store.getCurrentPage;
    switch (current) {
      case 'connectingVibrators':
        this.props.store.setCurrentPage('startPage');
        break;
      case 'configurationPage':
        this.props.store.setCurrentPage('startPage');
        break;
      case 'mainPage':
        if(this.props.store.navigation){
          this.props.store.changeNavigation();
        }else{
          this.props.store.setCurrentPage('configurationPage');
        }
        break;
    }
  }

  render() {
    const current = this.props.store.getCurrentPage;
    if( current == 'startPage' )
      return null;
    return (
      <View style={styles.containerStyle}>
        <TouchableWithoutFeedback
          onPress={() => this.changePage()}
        >
          <Icon
            type='entypo'
            name='chevron-small-left'
            size={50}
            color='#5AC8FA'
            iconStyle={{
              textShadowColor: 'rgba(0, 0, 0, 0.5)',
              textShadowOffset: {width: 1, height: 1},
              textShadowRadius: 7
            }}
          />
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    position: 'absolute',
    top: 17,
    left: -5
  }
});
