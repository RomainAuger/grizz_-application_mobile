import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';
import { inject, observer } from 'mobx-react';
import Display from 'react-native-display';

import StartPage from './screens/start-page';
import ConnectingVibrators from './screens/connecting-vibrators';
import ConfigurationPage from './screens/configuration-page';
import MainPage from './screens/main-page';

import RetourButton from './components/retour-button.js';
import Manoeuvre from './components/manoeuvre.js';

@inject(['store'])
@observer
export default class App extends React.Component {
  render() {
    console.log(this.props.store.currentPage);
    return (
      <View style={styles.container}>
        <Image
          style={styles.imgBgStyle}
          source={require('../assets/img/background.png')}
        />

          <StartPage />
          <ConnectingVibrators />
          <ConfigurationPage />
          <MainPage />

          <RetourButton />

          <Manoeuvre />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  imgBgStyle: {
    backgroundColor: '#ccc',
    flex: 1,
    resizeMode: 'cover',
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  }
});
