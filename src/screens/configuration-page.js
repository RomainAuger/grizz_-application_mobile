import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, Alert, Vibration } from 'react-native';
import { Button } from 'react-native-elements';
import { inject, observer } from 'mobx-react';

const SCREEN_WIDTH = Dimensions.get('window').width;
const BUTTON_HEIGHT = 50;

@inject(['store'])
@observer
export default class ConfigurationPage extends React.Component {
  render() {
    /* Test pour savoir s'il faut render la page */
    const currentPage = this.props.store.getCurrentPage;
    if (currentPage != 'configurationPage') return null;

    return (
      <View style={styles.container}>
        {/*Image du T-Shirt*/}
        <Image
          style={styles.shirtStyle}
          source={require('../../assets/img/shirt.png')}
          resizeMode="contain"
        />

        {/*Text*/}
        <View style={styles.containerTextStyle}>
          <Text style={styles.textStyle}>
            Avez-vous ressentie la vibration à droite ?
          </Text>
        </View>

        {/*Groupe des boutons*/}
        <View>
          {/*Groupe Oui et Non*/}
          <View style={styles.yesNoStyle}>
            <Button
              rounded
              title='Oui'
              buttonStyle={styles.buttonYesStyle}
              textStyle={styles.textStyle}
              onPress={() => this.props.store.setCurrentPage('mainPage')}
            />
            <Button
              rounded
              title='Non'
              buttonStyle={styles.buttonNoStyle}
              textStyle={styles.textStyle}
              onPress={() => this.props.store.setCurrentPage('mainPage')}
            />
          </View>
          {/*Bouton pour renvoyer une vibration*/}
          <View style={styles.newVibStyle}>
            <Button
              rounded
              title='Envoyer une nouvelle vibration'
              buttonStyle={styles.buttonNewVibStyle}
              textStyle={styles.textStyle}
              onPress={() => Vibration.vibrate(500)}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  shirtStyle: {
    marginTop: 60,
    width: '75%',
    height: '50%'
  },
  textStyle: {
    fontFamily: 'helveticaLt',
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    //lineHeight: 35
  },
  containerTextStyle: {
    width: '80%',
  },
  buttonNewVibStyle: {
    backgroundColor: '#5AC8FA',
    height: BUTTON_HEIGHT,
    width: 0.9 * SCREEN_WIDTH,
  },
  buttonYesStyle: {
    backgroundColor: '#4CD978',
    height: BUTTON_HEIGHT,
    width: 0.4 * SCREEN_WIDTH,
  },
  buttonNoStyle: {
    backgroundColor: '#FF3B30',
    height: BUTTON_HEIGHT,
    width: 0.4 * SCREEN_WIDTH,
  },
  yesNoStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'stretch',
    width: 0.9 * SCREEN_WIDTH + 30,
    marginTop: 20
  },
  newVibStyle: {
    marginBottom: 40,
    marginTop: 20
  },
});
