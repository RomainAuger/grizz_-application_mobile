import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Dimensions, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { inject, observer } from 'mobx-react';

const SCREEN_WIDTH = Dimensions.get('window').width;
const BUTTON_HEIGHT = 50;

@inject(['store'])
@observer
export default class ConnectingVibrators extends React.Component {
  state= {
    cmpt: 1,
    lancementTimer: false,
    isVibratorsConneceted: false
  }

  render() {
    /* Test pour savoir s'il faut render la page */
    const currentPage = this.props.store.getCurrentPage;
    if (currentPage != 'connectingVibrators') return null;

    if(this.state.lancementTimer == false){
      this.state.lancementTimer = true;
      setTimeout(() => {
        //this.props.store.setCurrentPage('startPage');
        this.setState({isVibratorsConneceted: true});
      },2000);
    }

    if(this.state.isVibratorsConneceted){
      return (
        <View style={styles.container}>
          <Image
            source={require('../../assets/img/check.png')}
            style={styles.checkStyle}
            resizeMode="contain"
          />
          <Text style={styles.textStyle}>Vibreurs connectés !</Text>
          <Button
            rounded
            title='Configuration des vibreurs'
            buttonStyle={styles.buttonNextButton}
            textStyle={styles.textStyle}
            onPress={() => this.props.store.setCurrentPage('configurationPage')}
          />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <Text style={styles.textStyle}>Connexion aux vibreurs bluetooth ...</Text>
        <ActivityIndicator size="large" color="white" style={styles.spinnerStyle} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textStyle: {
    fontFamily: 'helveticaLt',
    color: 'white',
    fontSize: 18
  },
  spinnerStyle: {
    marginTop: 20
  },
  checkStyle: {
    width: '60%',
    height: '60%'
  },
  buttonNextButton: {
    backgroundColor: '#4CD978',
    height: BUTTON_HEIGHT,
    width: 0.9 * SCREEN_WIDTH,
  },
});
