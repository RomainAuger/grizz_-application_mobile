import React from 'react';
import { StyleSheet, Text, Platform, View, Image, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { MapView, Location, Permissions } from 'expo';
import { inject, observer } from 'mobx-react';
import Display from 'react-native-display';
import Polyline from '@mapbox/polyline';

import NavBar from '../components/navbar';
import BottomBar from '../components/bottom-bar';
import DirectionsInput from '../components/directions-input';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const BUTTON_HEIGHT = 50;
const BUTTON_WIDTH = 80;
const GOOGLEMAP_API_KEY = 'AIzaSyCS7fRjjs9sjhb5EZBj_TCMZuDS-tD5-2Q';
const GOOGLEMAP_DIRECTIONS = 'https://maps.googleapis.com/maps/api/directions/json?';

const NB_ITERATION_MAX_BOUCLE = 100;

function getDistanceFromLatLonInM(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1);
  var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c; // Distance in km
  return d * 1000; // Distance in m
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function coordDistAngle(lat1, lon1, dist, angle){
  const R = 6371;

  lat1 = lat1 * Math.PI / 180;
  lon1 = lon1 * Math.PI / 180;

  const lat2 = Math.asin( Math.sin(lat1)*Math.cos(dist/R) + Math.cos(lat1)*Math.sin(dist/R)*Math.cos(angle) );
  const lon2 = lon1 + Math.atan2(Math.sin(angle)*Math.sin(dist/R)*Math.cos(lat1), Math.cos(dist/R)-Math.sin(lat1)*Math.sin(lat2));

  return {latitude: lat2 * 180 / Math.PI, longitude: lon2 * 180 / Math.PI};
}

@inject(['store'])
@observer
export default class MainPage extends React.Component {
  state = {
    coords: [],
    man_cmpt: 0
  }

  boucle = async (dist) => {
    const {latitude,longitude} = this.state.location.coords;

    const nbPoint = dist < 2 ? 5 : dist < 7 ? 10 : 22;
    console.log('Nb points : ', nbPoint);
    const angle = Math.random() * Math.PI;
    console.log('Angle : ',angle,' rad');
    let rayon = dist / (2 * Math.PI);
    console.log('Rayon du cercle : ', rayon);


    let cmpt = 1;
    let distTotal = 0;
    let coords = null;
    let coord_man = null;
    while( Math.abs(dist - distTotal) >= 0.1 * dist && cmpt < NB_ITERATION_MAX_BOUCLE){
      coord_man = [];

      console.log("iteration ", cmpt);
      console.log("rayon :", rayon);

      if(cmpt%5 == 0){
        rayon = dist / (2 * Math.PI);
      }

      const centreCercle = coordDistAngle(latitude,longitude,rayon,angle);
      const increment = (2 * Math.PI) / (nbPoint);
      let points = [];
      for(let i = 0; i < nbPoint ; i++){
        const tmp = coordDistAngle(centreCercle.latitude,centreCercle.longitude,rayon,angle + Math.PI + (i * increment));
        points[i] = [tmp.latitude,tmp.longitude];
      }

      const encoded = [];

      for(let i = 0; i < nbPoint ; i++){
        encoded[i] = Polyline.encode([points[i]]);
      }

      const start = latitude + ',' + longitude;

      let url = `${GOOGLEMAP_DIRECTIONS}origin=${start}&destination=${start}&waypoints=via:enc:${encoded[0]}:`;

      for(let i = 1; i < nbPoint ; i++){
        url = url + '|via:enc:' + encoded[i] + ':';
      }

      url = url + `&key=${GOOGLEMAP_API_KEY}&mode=walking`;

      let resp = await fetch(url);
      let respJson = await resp.json();

      const parcours = respJson.routes[0].legs;

      distTotal = 0;

      for(let i=0; i<parcours.length; i++){
        for(let j=0; j<parcours[i].steps.length; j++){
          distTotal = distTotal + parcours[i].steps[j].distance.value;
        }
      }

      for(let i=0; i<parcours.length; i++){
        for(let j=1; j<parcours[i].steps.length; j++){
          if(parcours[i].steps[j].maneuver == 'roundabout-right' || parcours[i].steps[j].maneuver == 'roundabout-left'){
            coord_man.push({option: parcours[i].steps[j].html_instructions.charAt(31),man: parcours[i].steps[j].maneuver ,coord: {latitude: parcours[i].steps[j].start_location.lat, longitude: parcours[i].steps[j].start_location.lng}, fini: 0});
          }else{
            coord_man.push({man: parcours[i].steps[j].maneuver ,coord: {latitude: parcours[i].steps[j].start_location.lat, longitude: parcours[i].steps[j].start_location.lng}, fini: 0});
          }
        }
      }

      distTotal = distTotal / 1000;

      console.log("distance totale", distTotal);

      let pointsBis = Polyline.decode(respJson.routes[0].overview_polyline.points);
      coords = pointsBis.map((point, index) => {
          return  {
              latitude : point[0],
              longitude : point[1]
          }
      })

      if(distTotal > 1.1 * dist){
        const dr = (distTotal - dist) / 2 * Math.PI;
        rayon = rayon - 0.1;
        console.log("dr :",dr);
      }
      else if(distTotal < 0.9 * dist){
        const dr = (dist - distTotal) / 2 * Math.PI;
        rayon = rayon + 0.1;
        console.log("dr :",dr);
      }

      cmpt = cmpt + 1;
    }

    this.setState({coords: coords, coord_man: coord_man, man_cmpt: 0, final_coord: coords[coords.length-1]});
    this.props.store.setCoords(coords);
    this.props.store.changeNavigation();

    const len = coords.length;
    const xa = coords[0].latitude;
    const ya = coords[0].longitude;
    const xb = coords[Math.floor(len/2)].latitude;
    const yb = coords[Math.floor(len/2)].longitude;

    const offx = xb - xa;
    const offy = yb - ya;

    const coef = 0.5;

    let newCoords = [];
    newCoords.push({latitude: xb + coef*offx, longitude: yb + coef*offy,});
    newCoords.push({latitude: xa - coef*offx, longitude: ya - coef*offy,});
    for(let i = 0; i<len; i++){
      newCoords.push(coords[i]);
    }

    this.map.fitToCoordinates(newCoords);

  }

  getCoords = async () => {
    const mode = this.props.store.getCurrentNavPage == 'bicycle' ? 'bicycling' : 'walking';
    let start = this.props.store.depart;
    const end = this.props.store.destination;
    if(end == '' || !(/\S/.test(end))){
      console.log('sljkfzopirejfzoiehjf');
      return;
    }
    if(start == 'Ma position'){
      const {latitude, longitude} = this.state.location.coords;
      start = latitude + ',' + longitude;
    }
    let resp = await fetch(`${GOOGLEMAP_DIRECTIONS}origin=${start}&destination=${end}&key=${GOOGLEMAP_API_KEY}&mode=${mode}`);
    let respJson = await resp.json();
    //console.log(respJson.routes[0].legs[0].steps);
    const test = respJson.routes[0].legs;

    const coord_man = [];

    for(let i=0; i<test.length; i++){
      for(let j=1; j<test[i].steps.length; j++){
        if(test[i].steps[j].maneuver == 'roundabout-right' || test[i].steps[j].maneuver == 'roundabout-left'){
          coord_man.push({option: test[i].steps[j].html_instructions.charAt(31),man: test[i].steps[j].maneuver ,coord: {latitude: test[i].steps[j].start_location.lat, longitude: test[i].steps[j].start_location.lng}, fini: 0});
        }else{
          coord_man.push({man: test[i].steps[j].maneuver ,coord: {latitude: test[i].steps[j].start_location.lat, longitude: test[i].steps[j].start_location.lng}, fini: 0});
        }
      }
    }

    let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
    let coords = points.map((point, index) => {
        return  {
            latitude : point[0],
            longitude : point[1]
        }
    })
    this.props.store.changeNavigation()
    this.setState({coords: coords, coord_man: coord_man, man_cmpt: 0, final_coord: coords[coords.length-1]});
    this.props.store.setCoords(coords);

    const len = coords.length;
    const xa = coords[0].latitude;
    const ya = coords[0].longitude;
    const xb = coords[len-1].latitude;
    const yb = coords[len-1].longitude;

    const offx = xb - xa;
    const offy = yb - ya;

    const coef = 0.25;

    let newCoords = [];
    newCoords.push({latitude: xb + coef*offx, longitude: yb + coef*offy,});
    newCoords.push({latitude: xa - coef*offx, longitude: ya - coef*offy,});
    for(let i = 0; i<len; i++){
      newCoords.push(coords[i]);
    }

    this.map.fitToCoordinates(newCoords);
  }

  getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission pour les coord refusée',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });

    this.map.fitToCoordinates([{latitude: location.coords.latitude, longitude: location.coords.longitude}]);

    Location.watchPositionAsync({
        enableHighAccuracy: true,
        timeInterval: 100,
      }, location => {
          this.setState({ location });

          //Check des manoeuvres
          let {coord_man, man_cmpt} = this.state;
          if(coord_man) {
            const current_pos = location.coords;
            const distMax = this.props.store.getCurrentNavPage == 'bicycle' ? 40 : 10;

            if(coord_man.length > 0){
              const next_man = coord_man[man_cmpt].coord;

              const dist = getDistanceFromLatLonInM(current_pos.latitude,current_pos.longitude,next_man.latitude,next_man.longitude);

              if(dist < distMax) {
                //console.log(coord_man[man_cmpt].man);

                if(coord_man[man_cmpt].option){
                  this.props.store.manoeuvre(coord_man[man_cmpt].man + ' ' + coord_man[man_cmpt].option + ' sortie');
                }else{
                  this.props.store.manoeuvre(coord_man[man_cmpt].man);
                }
                coord_man[man_cmpt].fini = 1;
                this.setState({man_cmpt: man_cmpt+1});
              }
            }

            const {final_coord} = this.state;
            const distFinal = getDistanceFromLatLonInM(current_pos.latitude,current_pos.longitude,final_coord.latitude,final_coord.longitude);
            if( distFinal < distMax && man_cmpt > 0){
              console.log('fini !!!');
              this.setState({coords: null, coord_man: null, man_cmpt: 0});
              this.props.store.changeNavigation();
            }
          }
     });
  };

  componentWillMount() {
    this.getLocationAsync();
  }

  render() {
    /* Test pour savoir s'il faut render la page */
    const currentPage = this.props.store.getCurrentPage;
    if (currentPage != 'mainPage') return null;

    //Pour savoir le mode de parcours
    const currentNavPage = this.props.store.getCurrentNavPage;

    //Pour savoir si on est en navigation
    const navigation = this.props.store.navigation;

    const markerStyle = this.state.location ? {
      width: 30,
      transform: [
        { rotate: `${this.state.location.coords.heading}deg` }
      ]
    } : null;

    return (
      <View style={styles.container}>
        <View style={styles.mapContainer}>
          <MapView
            provider='google'
            style={styles.mapViewStyle}
            ref={(ref) => { this.map = ref }}
          >
            <MapView.Polyline
              coordinates={this.state.coords}
              strokeWidth={6}
              strokeColor="#337bce"/>
            {this.state.final_coord ? <MapView.Marker coordinate={this.state.final_coord} /> : null}
            {this.state.location ? Platform.OS === 'ios' ? (
              <MapView.Marker anchor={{ x: 0.5, y: 0.5 }} coordinate={{latitude: this.state.location.coords.latitude, longitude: this.state.location.coords.longitude}}>
                <Image ref='image' resizeMode='contain' style={markerStyle} source={require('../../assets/img/current_position.png')}/>
              </MapView.Marker>)
              :
              (<MapView.Marker style={{transform: [{ rotate: `${this.state.location.coords.heading}deg` }]}} image={require('../../assets/img/current_position.png')} coordinate={{latitude: this.state.location.coords.latitude, longitude: this.state.location.coords.longitude}} />)
            : null}
            {this.state.points ? this.state.points.map((u,i) => <MapView.Marker key={i} coordinate={u} pinColor="#337bce"/>) : null}
          </MapView>

          <Display enable={!navigation} exit='fadeOut' enter='fadeIn' keepAlive style={styles.directionsInputStyle}>
            <DirectionsInput ref={ref => (this.directionInput = ref)} />
          </Display>

          <Display enable={!navigation} exit='fadeOut' enter='fadeIn' keepAlive style={{top: ((100-18)/100)*SCREEN_HEIGHT - 60,
              position: 'absolute', alignItems: 'center',}}>
            <Button
              rounded
              title='Go !'
              buttonStyle={styles.buttonNewVibStyle}
              textStyle={styles.textStyle}
              onPress={() => {
                if(currentNavPage != 'run'){
                  this.directionInput.wrappedInstance.getAdresses();
                  this.getCoords();
                }else{
                  const nb = this.directionInput.wrappedInstance.getNbKm();
                  if(nb != ''){
                    this.boucle(nb);
                  }
                }
              }}
            />
          </Display>
        </View>
        <BottomBar />
        <TouchableOpacity
          style={{backgroundColor: 'rgba(0,0,0,0.6)', position: 'absolute',bottom: '20%', right: 15 , opacity: 0.5, padding: 3, borderRadius: 14}}
          onPress={() => this.map.fitToCoordinates([{latitude: this.state.location.coords.latitude, longitude: this.state.location.coords.longitude}])}
        >
          <Icon
            type='font-awesome'
            name='crosshairs'
            size={50}
            color='white'
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textStyle: {
    fontFamily: 'helveticaLt',
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    //lineHeight: 35
  },
  directionsInputStyle: {
    position: 'absolute',
    top: 80,
    width: '100%',
    alignItems: 'center',
  },
  mapContainer: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  mapViewStyle: {
    position: 'absolute',
    width: '100%',
    height: SCREEN_HEIGHT,
    top: 0,
    zIndex: -1,
    flex: 1,
  },
  buttonNewVibStyle: {
    backgroundColor: '#5AC8FA',
    height: BUTTON_HEIGHT,
    width: BUTTON_WIDTH,
    shadowColor: 'black',
    shadowRadius: 7,
    shadowOffset: {width: 0,height: 0},
    shadowOpacity: 0.3
  },
});
