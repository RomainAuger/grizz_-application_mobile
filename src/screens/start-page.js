import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import { Button } from 'react-native-elements';
import { inject, observer } from 'mobx-react';

const SCREEN_WIDTH = Dimensions.get('window').width;
const BUTTON_HEIGHT = 50;

@inject(['store'])
@observer
export default class StartPage extends React.Component {
  render() {
    /* Test pour savoir s'il faut render la page */
    const currentPage = this.props.store.getCurrentPage;
    if (currentPage != 'startPage') return null;

    console.log(this.refs.button);

    return (
      <View style={styles.container} >
        <Image
          style={styles.logoStyle}
          source={require('../../assets/img/grizzlogo.png')}
          resizeMode="contain"
        />

        <View style={styles.buttonContainer} ref='button'>
          <View style={styles.dividerStyle} />
          <Button
            rounded
            title='Commencer'
            buttonStyle={styles.buttonStyle}
            textStyle={styles.textStyle}
            onPress={() => this.props.store.setCurrentPage('connectingVibrators')}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
  },
  buttonContainer: {
    flex: 1,
    marginTop: 70,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  logoStyle: {
    width: '55%',
    height: '62%',
  },
  dividerStyle: {
    position: 'absolute',
    top: BUTTON_HEIGHT/2 - 1,
    width: SCREEN_WIDTH,
    backgroundColor: '#fff',
    height: 1,
  },
  textStyle: {
    color: '#414464',
    fontFamily: 'helveticaLt',
    fontSize: 20
  },
  buttonStyle: {
    backgroundColor: 'white',
    height: BUTTON_HEIGHT,
    width: 0.7 * SCREEN_WIDTH,
  }
});
