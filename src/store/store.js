import { observable, computed , action } from 'mobx';

class Store {
  constructor() {
    this.currentPage = 'startPage';
    this.currentNavPage = 'walk';
  }

  /*Parti navigation*/
  @observable currentPage;

  @computed get getCurrentPage() {
    return this.currentPage;
  };

  @action setCurrentPage(p) {
    this.currentPage = p;
  }

  /*Parti Navbar*/
  @observable currentNavPage;

  @computed get getCurrentNavPage() {
    return this.currentNavPage;
  };

  @action setCurrentNavPage(p) {
    this.currentNavPage = p;
  }

  /*Parti Localisation*/
  @observable refs;

  @observable depart = 'Ma position';

  @observable destination = '';

  @observable coords = [];

  @action setCoords(c){
    this.coords = c;
  }

  /*Parti main page*/
  @observable navigation = false;

  @action changeNavigation() {
    this.navigation = !this.navigation;
  }

  /*Parti manoeuvre*/
  @observable manoeuvre;

}

const store = new Store();
export default store;
